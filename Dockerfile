FROM python:3.8.10-slim-buster
MAINTAINER jgehman@gehmanlab.com
# ref https://github.com/wemake-services/wemake-django-template/blob/master/%7B%7Bcookiecutter.project_name%7D%7D/docker/django/Dockerfile
# https://stackoverflow.com/questions/53835198/integrating-python-poetry-with-docker

ENV PYTHONFAULTHANDLER=1 \
  PYTHONUNBUFFERED=1 \
  PYTHONHASHSEED=random \
  PIP_NO_CACHE_DIR=off \
  PIP_DISABLE_PIP_VERSION_CHECK=on \
  PIP_DEFAULT_TIMEOUT=100 \
  POETRY_VERSION=1.1.8

RUN apt-get -y update
RUN apt-get install -yqq --no-install-recommends gcc musl-dev libc-dev libspatialindex-dev python3-dev libpq-dev build-essential texlive texlive-xetex texlive-generic-extra texlive-generic-recommended cm-super
RUN python3 -m pip install --upgrade pip
# Setting Home Directory for containers

WORKDIR /root/proj
RUN mkdir work
RUN mkdir DataVolume

COPY requirements.txt .

RUN pip install -r requirements.txt

# Add Tini. Tini operates as a process subreaper for jupyter. This prevents kernel crashes.
ENV TINI_VERSION v0.6.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /usr/bin/tini
RUN chmod +x /usr/bin/tini
ENTRYPOINT ["/usr/bin/tini", "--"]

# Jupyter NB extensions
RUN jupyter contrib nbextension install --system
RUN jupyter nbextensions_configurator enable --system
RUN jupyter nbextension enable codefolding/main
RUN jupyter nbextension enable codefolding/edit
RUN jupyter nbextension enable toc2/main
RUN jupyter nbextension enable python-markdown/main
RUN jupyter nbextension enable collapsible_headings/main

#CMD ["cd", "work"]
CMD ["jupyter", "notebook", "--port=8888", "--no-browser", "--ip=0.0.0.0", "--allow-root"]
